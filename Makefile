# Copyright © 2018-2020 Johnothan King. All rights reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

CC      := cc
STRIP   := strip
CFLAGS  := -Os -fomit-frame-pointer -fpic -fno-plt -D_FORTIFY_SOURCE=2 -pipe
WFLAGS  := -Wall -Wextra -Wpedantic
LDFLAGS := -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now

all:
	if [ `uname` = SunOS ]; then $(CC) $(CFLAGS) $(WFLAGS) -D`uname` -o tinyinit init.c $(LDFLAGS); else $(CC) $(CFLAGS) $(WFLAGS) -D`uname` -o tinyinit init.c $(LDFLAGS) -lutil; fi
	$(STRIP) --strip-unneeded -R .comment -R .gnu.version -R .GCC.command.line -R .note.gnu.gold-version tinyinit || true
install: all
	install -Dm0755 tinyinit "$(DESTDIR)/sbin/tinyinit"
clean:
	rm -f tinyinit
