/*
 * Copyright © 2018-2020 Johnothan King. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* tinyinit -- A very basic init program with a high level of portability */
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#if defined(OpenBSD) || defined(NetBSD)
#include <util.h>
#elif FreeBSD
#include <libutil.h>
#else
#include <utmp.h>
#endif
#ifdef SunOS
#define loginTTY dup
#else
#define loginTTY login_tty
#endif

int main(void) {
    /* Return 1 when re-executed */
    if (getpid() != 1) return 1;

    /* Launch a shell */
    if (fork() == 0) {
        loginTTY(open("/dev/console", O_RDWR));
        return execl("/bin/sh", "/bin/sh", (char*)0);
    }

    /* Kill all zombie processes */
    for (;;) wait((int*)0);
}
